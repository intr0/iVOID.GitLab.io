## https://ko-fi.com/intr0

[![Latest Release](https://gitlab.com/intr0/iVOID.GitLab.io/-/badges/release.svg)](https://gitlab.com/intr0/iVOID.GitLab.io/-/releases)

[![pipeline status](https://gitlab.com/intr0/iVOID.GitLab.io/badges/master/pipeline.svg)](https://gitlab.com/intr0/iVOID.GitLab.io/-/commits/master)

***
[![FOSSA Status](https://app.fossa.com/api/projects/git%2Bgitlab.com%2Fintr0%2FiVOID.GitLab.io.svg?type=shield)](https://app.fossa.com/projects/git%2Bgitlab.com%2Fintr0%2FiVOID.GitLab.io?ref=badge_shield) 
***
### Now proudly included on: [FilterLists.com](https://filterlists.com/).
**`https://filterlists.com/`**
***
# **[iVOID](https://GitLab.com/intr0/iVOID.GitLab.io)**
#### iVOID.hosts was created and is maintained by intr0 for use on iOS devices using AdGuard Pro, which is FOSS. It also serves well as a standard workstation hosts file and with uBlock Origin or AdGuard for Firefox. iVOID.hosts protects you from ads, tracking, resource hijacking including crypto-jacking, malware in general & phishing as generously licensed to us by Hardenize.com.
***
### [AdGuard for iOS Pro](https://adguard.com/en/adguard-ios-pro/overview.html) is Free and Open-Source Software.
**`https://adguard.com/en/adguard-ios-pro/overview.html`**
## See [Free Software Foundation](https://FSF.org/) to learn more.
**`https://FSF.org`**
***
## GET OUR HOSTS FILE on GitLab: [iVOID.hosts](https://gitlab.com/intr0/iVOID.GitLab.io/raw/master/iVOID.hosts)
**`https://gitlab.com/intr0/iVOID.GitLab.io/raw/master/iVOID.hosts`**
***
## Our **NotABug.org** Repo: [iVOID.hosts](https://notabug.org/-intr0/iVOID)
**`https://notabug.org/-intr0/iVOID`**
## GET OUR HOSTS FILE on NotABug: [iVOID.hosts](https://notabug.org/-intr0/iVOID/raw/master/iVOID.hosts)
**`https://notabug.org/-intr0/iVOID/raw/master/iVOID.hosts`**
